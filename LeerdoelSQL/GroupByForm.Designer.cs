﻿namespace LeerdoelSQL
{
    partial class GroupByForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridViewEmployee = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salaryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.employeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSEmployee = new LeerdoelSQL.DSEmployee();
            this.employeeTableAdapter = new LeerdoelSQL.DSEmployeeTableAdapters.EmployeeTableAdapter();
            this.dSEmployeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btn_filter = new System.Windows.Forms.Button();
            this.dataGridViewAvgSal = new System.Windows.Forms.DataGridView();
            this.dsAdvisor1 = new LeerdoelSQL.DSAdvisor();
            this.btn_back = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSEmployeeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAvgSal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsAdvisor1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewEmployee
            // 
            this.dataGridViewEmployee.AllowUserToAddRows = false;
            this.dataGridViewEmployee.AllowUserToDeleteRows = false;
            this.dataGridViewEmployee.AutoGenerateColumns = false;
            this.dataGridViewEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEmployee.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.salaryDataGridViewTextBoxColumn});
            this.dataGridViewEmployee.DataSource = this.employeeBindingSource;
            this.dataGridViewEmployee.Location = new System.Drawing.Point(36, 23);
            this.dataGridViewEmployee.Name = "dataGridViewEmployee";
            this.dataGridViewEmployee.ReadOnly = true;
            this.dataGridViewEmployee.Size = new System.Drawing.Size(247, 150);
            this.dataGridViewEmployee.TabIndex = 0;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // salaryDataGridViewTextBoxColumn
            // 
            this.salaryDataGridViewTextBoxColumn.DataPropertyName = "salary";
            this.salaryDataGridViewTextBoxColumn.HeaderText = "salary";
            this.salaryDataGridViewTextBoxColumn.Name = "salaryDataGridViewTextBoxColumn";
            this.salaryDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // employeeBindingSource
            // 
            this.employeeBindingSource.DataMember = "Employee";
            this.employeeBindingSource.DataSource = this.dSEmployee;
            // 
            // dSEmployee
            // 
            this.dSEmployee.DataSetName = "DSEmployee";
            this.dSEmployee.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // employeeTableAdapter
            // 
            this.employeeTableAdapter.ClearBeforeFill = true;
            // 
            // dSEmployeeBindingSource
            // 
            this.dSEmployeeBindingSource.DataSource = this.dSEmployee;
            this.dSEmployeeBindingSource.Position = 0;
            // 
            // btn_filter
            // 
            this.btn_filter.Location = new System.Drawing.Point(66, 190);
            this.btn_filter.Name = "btn_filter";
            this.btn_filter.Size = new System.Drawing.Size(187, 23);
            this.btn_filter.TabIndex = 1;
            this.btn_filter.Text = "Show average salary per name:";
            this.btn_filter.UseVisualStyleBackColor = true;
            this.btn_filter.Click += new System.EventHandler(this.btn_filter_Click);
            // 
            // dataGridViewAvgSal
            // 
            this.dataGridViewAvgSal.AllowUserToAddRows = false;
            this.dataGridViewAvgSal.AllowUserToDeleteRows = false;
            this.dataGridViewAvgSal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAvgSal.Location = new System.Drawing.Point(36, 231);
            this.dataGridViewAvgSal.Name = "dataGridViewAvgSal";
            this.dataGridViewAvgSal.ReadOnly = true;
            this.dataGridViewAvgSal.Size = new System.Drawing.Size(247, 150);
            this.dataGridViewAvgSal.TabIndex = 2;
            // 
            // dsAdvisor1
            // 
            this.dsAdvisor1.DataSetName = "DSAdvisor";
            this.dsAdvisor1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btn_back
            // 
            this.btn_back.Location = new System.Drawing.Point(117, 401);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(75, 23);
            this.btn_back.TabIndex = 3;
            this.btn_back.Text = "back";
            this.btn_back.UseVisualStyleBackColor = true;
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // GroupByForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 436);
            this.Controls.Add(this.btn_back);
            this.Controls.Add(this.dataGridViewAvgSal);
            this.Controls.Add(this.btn_filter);
            this.Controls.Add(this.dataGridViewEmployee);
            this.Name = "GroupByForm";
            this.Text = "GroupByForm";
            this.Load += new System.EventHandler(this.GroupByForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSEmployeeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAvgSal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsAdvisor1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewEmployee;
        private DSEmployee dSEmployee;
        private System.Windows.Forms.BindingSource employeeBindingSource;
        private DSEmployeeTableAdapters.EmployeeTableAdapter employeeTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn salaryDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource dSEmployeeBindingSource;
        private System.Windows.Forms.Button btn_filter;
        private System.Windows.Forms.DataGridView dataGridViewAvgSal;
        private DSAdvisor dsAdvisor1;
        private System.Windows.Forms.Button btn_back;
    }
}