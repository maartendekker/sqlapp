﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeerdoelSQL
{
    public partial class GroupByForm : Form
    {
        public GroupByForm()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void GroupByForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dSEmployee.Employee' table. You can move, or remove it, as needed.
            this.employeeTableAdapter.Fill(this.dSEmployee.Employee);

        }

        public void btn_filter_Click(object sender, EventArgs e)
        {
            string query = "SELECT name as Name, AVG(salary) as AverageSalary FROM [SQL].Employee " +
                           "GROUP BY name";
            var conn = new SqlConnection("Server=mssql.fhict.local;Database=dbi407381;User Id=dbi407381;Password=Password1234;");
            var dataAdapter = new SqlDataAdapter(query, conn);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            var ds = new DataSet();
            dataAdapter.Fill(ds);
            dataGridViewAvgSal.ReadOnly = true;
            dataGridViewAvgSal.DataSource = ds.Tables[0];
        }

        public void btn_back_Click(object sender, EventArgs e)
        {
            Start start = new Start();
            start.Show();
            this.Close();
        }
    }
}
