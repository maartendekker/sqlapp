﻿namespace LeerdoelSQL
{
    partial class OuterJoinForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridViewEmployees = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.advisorIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.employeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSEmployeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSEmployee = new LeerdoelSQL.DSEmployee();
            this.employeeTableAdapter = new LeerdoelSQL.DSEmployeeTableAdapters.EmployeeTableAdapter();
            this.dataGridViewAdvisors = new System.Windows.Forms.DataGridView();
            this.advisorIdDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.advisorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSAdvisor = new LeerdoelSQL.DSAdvisor();
            this.advisorTableAdapter = new LeerdoelSQL.DSAdvisorTableAdapters.AdvisorTableAdapter();
            this.btn_outerjoin = new System.Windows.Forms.Button();
            this.dataGridViewOuterJoin = new System.Windows.Forms.DataGridView();
            this.btn_back = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEmployees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSEmployeeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAdvisors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advisorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSAdvisor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOuterJoin)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewEmployees
            // 
            this.dataGridViewEmployees.AllowUserToAddRows = false;
            this.dataGridViewEmployees.AllowUserToDeleteRows = false;
            this.dataGridViewEmployees.AutoGenerateColumns = false;
            this.dataGridViewEmployees.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEmployees.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.advisorIdDataGridViewTextBoxColumn});
            this.dataGridViewEmployees.DataSource = this.employeeBindingSource;
            this.dataGridViewEmployees.Location = new System.Drawing.Point(12, 12);
            this.dataGridViewEmployees.Name = "dataGridViewEmployees";
            this.dataGridViewEmployees.ReadOnly = true;
            this.dataGridViewEmployees.Size = new System.Drawing.Size(245, 150);
            this.dataGridViewEmployees.TabIndex = 0;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // advisorIdDataGridViewTextBoxColumn
            // 
            this.advisorIdDataGridViewTextBoxColumn.DataPropertyName = "advisorId";
            this.advisorIdDataGridViewTextBoxColumn.HeaderText = "advisorId";
            this.advisorIdDataGridViewTextBoxColumn.Name = "advisorIdDataGridViewTextBoxColumn";
            this.advisorIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // employeeBindingSource
            // 
            this.employeeBindingSource.DataMember = "Employee";
            this.employeeBindingSource.DataSource = this.dSEmployeeBindingSource;
            // 
            // dSEmployeeBindingSource
            // 
            this.dSEmployeeBindingSource.DataSource = this.dSEmployee;
            this.dSEmployeeBindingSource.Position = 0;
            // 
            // dSEmployee
            // 
            this.dSEmployee.DataSetName = "DSEmployee";
            this.dSEmployee.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // employeeTableAdapter
            // 
            this.employeeTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridViewAdvisors
            // 
            this.dataGridViewAdvisors.AllowUserToAddRows = false;
            this.dataGridViewAdvisors.AllowUserToDeleteRows = false;
            this.dataGridViewAdvisors.AutoGenerateColumns = false;
            this.dataGridViewAdvisors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAdvisors.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.advisorIdDataGridViewTextBoxColumn1,
            this.nameDataGridViewTextBoxColumn1});
            this.dataGridViewAdvisors.DataSource = this.advisorBindingSource;
            this.dataGridViewAdvisors.Location = new System.Drawing.Point(274, 12);
            this.dataGridViewAdvisors.Name = "dataGridViewAdvisors";
            this.dataGridViewAdvisors.ReadOnly = true;
            this.dataGridViewAdvisors.Size = new System.Drawing.Size(245, 150);
            this.dataGridViewAdvisors.TabIndex = 1;
            // 
            // advisorIdDataGridViewTextBoxColumn1
            // 
            this.advisorIdDataGridViewTextBoxColumn1.DataPropertyName = "advisorId";
            this.advisorIdDataGridViewTextBoxColumn1.HeaderText = "advisorId";
            this.advisorIdDataGridViewTextBoxColumn1.Name = "advisorIdDataGridViewTextBoxColumn1";
            this.advisorIdDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn1
            // 
            this.nameDataGridViewTextBoxColumn1.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn1.HeaderText = "name";
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            this.nameDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // advisorBindingSource
            // 
            this.advisorBindingSource.DataMember = "Advisor";
            this.advisorBindingSource.DataSource = this.dSAdvisor;
            // 
            // dSAdvisor
            // 
            this.dSAdvisor.DataSetName = "DSAdvisor";
            this.dSAdvisor.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // advisorTableAdapter
            // 
            this.advisorTableAdapter.ClearBeforeFill = true;
            // 
            // btn_outerjoin
            // 
            this.btn_outerjoin.Location = new System.Drawing.Point(128, 178);
            this.btn_outerjoin.Name = "btn_outerjoin";
            this.btn_outerjoin.Size = new System.Drawing.Size(261, 23);
            this.btn_outerjoin.TabIndex = 2;
            this.btn_outerjoin.Text = "Show employees and advisors  that have no link";
            this.btn_outerjoin.UseVisualStyleBackColor = true;
            this.btn_outerjoin.Click += new System.EventHandler(this.btn_outerjoin_Click);
            // 
            // dataGridViewOuterJoin
            // 
            this.dataGridViewOuterJoin.AllowUserToAddRows = false;
            this.dataGridViewOuterJoin.AllowUserToDeleteRows = false;
            this.dataGridViewOuterJoin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOuterJoin.Location = new System.Drawing.Point(139, 216);
            this.dataGridViewOuterJoin.Name = "dataGridViewOuterJoin";
            this.dataGridViewOuterJoin.ReadOnly = true;
            this.dataGridViewOuterJoin.Size = new System.Drawing.Size(240, 150);
            this.dataGridViewOuterJoin.TabIndex = 3;
            // 
            // btn_back
            // 
            this.btn_back.Location = new System.Drawing.Point(433, 343);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(75, 23);
            this.btn_back.TabIndex = 4;
            this.btn_back.Text = "back";
            this.btn_back.UseVisualStyleBackColor = true;
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // OuterJoinForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 389);
            this.Controls.Add(this.btn_back);
            this.Controls.Add(this.dataGridViewOuterJoin);
            this.Controls.Add(this.btn_outerjoin);
            this.Controls.Add(this.dataGridViewAdvisors);
            this.Controls.Add(this.dataGridViewEmployees);
            this.Name = "OuterJoinForm";
            this.Text = "OuterJoin";
            this.Load += new System.EventHandler(this.OuterJoinForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEmployees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSEmployeeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAdvisors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advisorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSAdvisor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOuterJoin)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewEmployees;
        private System.Windows.Forms.BindingSource dSEmployeeBindingSource;
        private DSEmployee dSEmployee;
        private System.Windows.Forms.BindingSource employeeBindingSource;
        private DSEmployeeTableAdapters.EmployeeTableAdapter employeeTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn advisorIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dataGridViewAdvisors;
        private DSAdvisor dSAdvisor;
        private System.Windows.Forms.BindingSource advisorBindingSource;
        private DSAdvisorTableAdapters.AdvisorTableAdapter advisorTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn advisorIdDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.Button btn_outerjoin;
        private System.Windows.Forms.DataGridView dataGridViewOuterJoin;
        private System.Windows.Forms.Button btn_back;
    }
}