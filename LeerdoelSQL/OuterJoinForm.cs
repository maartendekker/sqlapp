﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeerdoelSQL
{
    public partial class OuterJoinForm : Form
    {
        public OuterJoinForm()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void OuterJoinForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dSAdvisor.Advisor' table. You can move, or remove it, as needed.
            this.advisorTableAdapter.Fill(this.dSAdvisor.Advisor);
            // TODO: This line of code loads data into the 'dSEmployee.Employee' table. You can move, or remove it, as needed.
            this.employeeTableAdapter.Fill(this.dSEmployee.Employee);

        }

        private void btn_outerjoin_Click(object sender, EventArgs e)
        {
            string query = "SELECT E.name AS Employee, A.name AS Advisor FROM[SQL].Employee E " +
                           "FULL OUTER JOIN[SQL].Advisor A ON E.advisorId = A.advisorId " +
                           "WHERE E.advisorId IS NULL OR A.advisorId IS NULL";
            var conn = new SqlConnection("Server=mssql.fhict.local;Database=dbi407381;User Id=dbi407381;Password=Password1234;");
            var dataAdapter = new SqlDataAdapter(query, conn);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            var ds = new DataSet();
            dataAdapter.Fill(ds);
            dataGridViewOuterJoin.ReadOnly = true;
            dataGridViewOuterJoin.DataSource = ds.Tables[0];
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            Start start = new Start();
            start.Show();
            this.Close();
        }
    }
}
