﻿namespace LeerdoelSQL
{
    partial class Start
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btn_groupby = new System.Windows.Forms.Button();
            this.btn_outerjoin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_groupby
            // 
            this.btn_groupby.Location = new System.Drawing.Point(12, 12);
            this.btn_groupby.Name = "btn_groupby";
            this.btn_groupby.Size = new System.Drawing.Size(200, 200);
            this.btn_groupby.TabIndex = 0;
            this.btn_groupby.Text = "Group By";
            this.btn_groupby.UseVisualStyleBackColor = true;
            this.btn_groupby.Click += new System.EventHandler(this.btn_groupby_Click);
            // 
            // btn_outerjoin
            // 
            this.btn_outerjoin.Location = new System.Drawing.Point(233, 12);
            this.btn_outerjoin.Name = "btn_outerjoin";
            this.btn_outerjoin.Size = new System.Drawing.Size(200, 200);
            this.btn_outerjoin.TabIndex = 1;
            this.btn_outerjoin.Text = "Outer Join";
            this.btn_outerjoin.UseVisualStyleBackColor = true;
            this.btn_outerjoin.Click += new System.EventHandler(this.btn_outerjoin_Click);
            // 
            // Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 226);
            this.Controls.Add(this.btn_outerjoin);
            this.Controls.Add(this.btn_groupby);
            this.Name = "Start";
            this.Text = "LeerdoelSQL";
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btn_groupby;
        private System.Windows.Forms.Button btn_outerjoin;
    }
}

