﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeerdoelSQL
{
    public partial class Start : Form
    {
        public Start()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        public void btn_groupby_Click(object sender, EventArgs e)
        {
            this.Hide();
            GroupByForm group = new GroupByForm();
            group.Show();
            
        }

        public void btn_outerjoin_Click(object sender, EventArgs e)
        {
            this.Hide();
            OuterJoinForm outerJoinForm = new OuterJoinForm();
            outerJoinForm.Show();
        }
    }
}
